// How many times the colour sensor have to read a certain
// colour in order to believe its input is correct.
#define COLOUR_COUNT_REDUNDANCY 5

// Set belt PWM. 50 is max.
#define BELT_MOTOR_PWM 50

#include "Nxt.h"
#include "Lcd.h"
#include "NxtColorSensor.h"
#include "Speaker.h"
#include "Clock.h"
#include "Motor.h"
#include "Colour.h"
#include "queue.cpp"
#include "LegoBrick.h"
#include "BucketController.h"

using namespace ecrobot;

extern "C"
{
    /* C Includes and global variables */
    #include "kernel.h"
    #include "kernel_id.h"
    #include "ecrobot_interface.h"
    
    short previousColour = 99, latestValidColour = 99, colourCounter = 0;
    int totalTaskTime = 0, leCurrentColour = 99, currentClock = 0;
    LegoBrick currentBrick;
    S16 rgb[3]; // Array containing the RGB values of the current colour.
    char *colourMap[8] = {
        "Black", "Blue", "Green", "Yellow", 
        "Orange", "Red", "White", "Unknown"
    };
    
    // periode test
    int startTime = 0;
    int time = 0;
    int fiveReading = 99;
     
    /* Prototypes */
    void DecideBrickColour(int, int);
    void turnBucket(int);
    
    /* TOPPERS/ATK declarations */
    DeclareCounter(SysTimerCnt);
    DeclareAlarm(ColourPollingAlarm);
    DeclareAlarm(ColourControllerAlarm);

    /* Initialize NXT devices, ports and utilities */
   // Nxt nxt;
    Clock clock;
    NxtColorSensor colourSensor(PORT_1, NxtColorSensor::_COLORSENSOR);
	Motor motorA(PORT_A);
	Motor motorC(PORT_C);
    
    
    

	/* nxtOSEK hooks */
	void ecrobot_device_initialize(){ ecrobot_init_bt_slave("LEJOS-OSEK"); }
	void ecrobot_device_terminate(){ ecrobot_term_bt_connection(); }

    /* nxtOSEK hook to be invoked from an ISR in category 2 */
    /* Increment OSEK Alarm Counter */
    void user_1ms_isr_type2(void){ (void)SignalCounter(SysTimerCnt); }

    ///=======================================================
    /// TASKS STARTS HERE... 
    ///=======================================================
    
    /* Starting task. Runs only once. */
    TASK(InitializationTask)
    {
        Lcd lcd;
        // Initialize the colour sensor and wait for it to be ready.
        colourSensor.processBackground(); 
        clock.wait(2000);
        
        // Initialize the belt motor.
		motorA.setPWM(BELT_MOTOR_PWM);
        TerminateTask();
    }
    
    /* Poll the colour sensor */
    TASK(ColourPollingTask)
    {
        /* processBackground communicates with colourSensor
        (this must be executed repeatedly in a background Task) */
        colourSensor.processBackground(); 
        TerminateTask();
    }
    
    /* Handle the data recieved from colour sensor */
    TASK(ColourControllerTask)
    {   
        
        // save current colour and clock.
        colourSensor.getRawColor(rgb);
        leCurrentColour = colourSensor.getColorNumber();
        currentClock = clock.now();
        
        // if the colour is orange or black, we ignore it.
        // This is due to sensor is inaccuracy.
        if(leCurrentColour == 4 || leCurrentColour == 0 )
            TerminateTask();
        
        DecideBrickColour(leCurrentColour, currentClock);
        
        TerminateTask();
    }

    
    /* Update LCD display for debugging */
    /*TASK(ShowDisplayTask)
    {
        Lcd lcd;
        
        // Clear the LCD buffer
        lcd.clear();
        
        // Print the colour estimated by the colour sensor        
        lcd.cursor(0,0);
        lcd.putf("ss", "COLOR: ", colourMap[leCurrentColour]);
        
        // Print raw RGB values from colour sensor on a single line
        lcd.cursor(0,1);
        lcd.putf("sd", "R", rgb[0],0);
        lcd.cursor(5,1);
        lcd.putf("sd", "G", rgb[1],0);
        lcd.cursor(10,1);
        lcd.putf("sd", "B", rgb[2],0);
        
        // Print the latest valid colour
        lcd.cursor(0,2);
        lcd.putf("ss", "LVC: ", colourMap[latestValidColour]);
        
        // Print the amount of times eg uontirw
        lcd.cursor(0,3);
        lcd.putf("sd", "CNT_CLR: ", colourCounter);
        
        // Print the time used by the measured Task
        lcd.cursor(0,4);
        lcd.putf("sd", "TASK_TIME: ", totalTaskTime);
        
        // Display stuff in LCD buffer
        lcd.disp();
        
        TerminateTask();
    }
    */
    ///=======================================================
    /// FUNCTIONS STARTS HERE... 
    ///=======================================================
    void DecideBrickColour(int CurrentColour, int currentClock)
    {
        Lcd lcd;
        int endTime = 0;
        // If the detected colour is not unknown, then something
        // should be done. Else there are no bricks being counted
        // and the counter should be reset.
        if(CurrentColour == previousColour)
        {
            colourCounter++;
            // To make op for the colour sensor inprecesion, 5 reading of the same colour should be used.
            if(colourCounter >= 5)
            {
                // Beginning of a new brick or end of the current.
                if(fiveReading != CurrentColour && CurrentColour != 99)
                {
                    startTime = clock.now();
                    latestValidColour = CurrentColour;
                    LegoBrick brick = {CurrentColour, currentClock};
                    currentBrick = brick;
                    colourCounter = 0;
                }
                else{
                    colourCounter = 0;
                    if(fiveReading != CurrentColour){
                        endTime = clock.now();
                        time = endTime - startTime;
                        motorC.setCount(colourCounter);
                        ecrobot_bt_data_logger(time/100, fiveReading);
                    }
                }
                // update to the new colour after the last 5 concurrent readings
                fiveReading = CurrentColour;
                
            }     
        }
        else {
            colourCounter = 0;
            previousColour = CurrentColour;
        }
         /*
        if(CurrentColour != 99)
        {
            if(CurrentColour != previousColour && previousColour == 2){
                endTime = clock.now();
                time = endTime - startTime;
                motorC.setCount(colourCounter);
                ecrobot_bt_data_logger(time/100, CurrentColour);
            }
            if(CurrentColour == previousColour)
            {
                colourCounter++;
                
                if(colourCounter >= COLOUR_COUNT_REDUNDANCY)
                {
                        startTime = clock.now();
                        latestValidColour = CurrentColour;
                        LegoBrick brick = {CurrentColour, currentClock};
                        currentBrick = brick;
                }
            }
            else if(CurrentColour != previousColour)
            {
                colourCounter = 0;
                previousColour = CurrentColour;
            }
        }
        else
        {
            colourCounter = 0;
        }
        */
    }
    
    
}
