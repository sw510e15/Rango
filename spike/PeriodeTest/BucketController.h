#ifndef BUCKET_CONTROLLER_H
#define BUCKET_CONTROLLER
#define RED_ANGLE 0
#define GREEN_ANGLE 180
#define BLUE_ANGLE 90
#define ERROR_ANGLE 270
#define BRAKELENGTH_50PWM 23
#define BRAKELENGTH_TESTSHORT_50PWM 3
#define BRAKELENGTH_TESTLONG_50PWM 43
#define ERROR_MARGIN 2
#define MOTOR_PWM 50

#include "Motor.h"
#include "Clock.h"
#include "Colour.h"
#include "NxtColorSensor.h"
#include "Lcd.h"


class BucketController {
public:
	BucketController();
	~BucketController();
	void TurnBucket(colour c);
	
private:
	void TurnMotorGreen ();
	void TurnMotorRed ();
	void BucketSafety (int angle);
};

#endif // BUCKET_CONTROLLER
