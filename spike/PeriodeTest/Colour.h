/* This header file serves make sure the code gets more readable
 * and writeable
 * USE WITH CARE
 * //Rene
*/
#ifndef Colour_h
#define Colour_H
#include "NxtColorSensor.h"

typedef ecrobot::NxtColorSensor::eColorNumber colour;

#define BLUE ecrobot::NxtColorSensor::_BLUE 
#define RED  ecrobot::NxtColorSensor::_RED 
#define GREEN ecrobot::NxtColorSensor::_GREEN 
#define UNKNOWN ecrobot::NxtColorSensor::_UNKNOWN

#endif
