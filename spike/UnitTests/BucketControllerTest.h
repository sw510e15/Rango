#ifndef BUCKET_CONTROLLER_TEST_H
#define BUCKET_CONTROLLER_TEST

#include "../BucketController/Colour.h"
#include "Motor.h"

class BucketControllerTest {
public:
	BucketControllerTest();
	~BucketControllerTest();
	void BucketEndlessTest();
	void BucketSystemTest();
	
private:
	char* ColourToString(colour c);
	int FindColourAngle(colour c);
	int Assert(int goalAngle, int endAngle, colour c);
};

#endif // BUCKET_CONTROLLER
