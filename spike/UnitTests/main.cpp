// This is the unit test class

// ECRobot++ API
#include "Motor.h"
#include "Nxt.h"
#include "Clock.h"
#include "Lcd.h"
#include "BucketControllerTest.h"
using namespace ecrobot;

extern "C"
{
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"
using namespace std; // maybe not necessary

void ecrobot_device_initialize()
{
	ecrobot_init_bt_slave("LEJOS-OSEK");
}

void ecrobot_device_terminate()
{
	ecrobot_term_bt_connection();
}

/* nxtOSEK hook to be invoked from an ISR in category 2 */
void user_1ms_isr_type2(void)
{
	SleeperMonitor(); // needed for I2C device and Clock classes
}

Motor motorA(PORT_A);

TASK(TaskMain)
{
	Nxt nxt;
	Clock clock;
	Lcd lcd;
	
	
	// BucketController tests
	BucketControllerTest bct;
	bct.BucketSystemTest();
	
	// ecrobot_bt_data_logger(pow, motorA.getCount() / 180);
	
	// ColourSensor tests
	
	
	
	lcd.clear();
	lcd.putf("s", "done");
	lcd.disp();
	clock.wait(2000);
	
	TerminateTask();
}

}



