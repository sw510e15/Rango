// ECRobot++ API
#include "Motor.h"
#include "Nxt.h"
#include "Clock.h"
#include "Lcd.h"
#include "../BucketController/BucketController.h"
using namespace ecrobot;

#include "BucketControllerTest.h"

Motor motor(PORT_A);

BucketControllerTest::BucketControllerTest() {}
BucketControllerTest::~BucketControllerTest() {}

void BucketControllerTest::BucketSystemTest()
{
	Nxt nxt;
	Clock clock;
	Lcd lcd;
	lcd.clear();
	BucketController bc;
	
	// init test array of colours
	colour colours[] = { GREEN,GREEN,ERROR,BLUE,RED,RED,GREEN,BLUE,ERROR,RED,GREEN,RED,GREEN,RED,GREEN,RED,
		RED,GREEN,RED,GREEN,RED,RED,GREEN,GREEN, RED };
	
	// test results array, 1 means success, 0 means failure
	int results[25];
	
	// perform test
	int length = sizeof(colours) / sizeof(colour);
	for (int i = 0; i < length; i++)
	{
		colour c = colours[i];
		int goalAngle, endAngle;
		
		lcd.clear();
		lcd.putf("sn", ColourToString(c));
		lcd.disp();
		
		// find target angle
		goalAngle = FindColourAngle(c);
		
		// perform test
		bc.TurnBucket(c);
		
		// give the bucket safety time to settle
		clock.wait(150);
		
		// find actual angle
		endAngle = motor.getCount() % 360;
		
		// check if endAngle is within error margin
		results[i] = Assert(goalAngle, endAngle, c);
		
		clock.wait(1000);
	}
	
	// doesnt work well
	// char resStr[25];
	// for (int i = 0; i < length; i++) {
		// resStr[i] = (char) results[i];
	// }
	
	// lcd.clear();
	// lcd.putf("s", resStr, 25);
	// lcd.disp();
	
	// clock.wait(5000);
	
	TerminateTask();
}


// public
void BucketControllerTest::BucketEndlessTest()
{
	Clock clock;
	Lcd lcd;

	lcd.clear();
	colour currentColour = GREEN;
	BucketController bc;
	while (1)
	{
		lcd.clear();
		lcd.putf("sn", ColourToString(currentColour));
		lcd.disp();
		bc.TurnBucket(currentColour);
		clock.wait(2000);
		currentColour = currentColour == RED ? GREEN : RED;
	}
}

// private
int BucketControllerTest::Assert (int goalAngle, int endAngle, colour c) {
	if (c == RED) {
		// red needs special check, because the endAngle may wrap around 360
		if (endAngle >= 0 && endAngle <= 0 + ERROR_MARGIN || endAngle <= 360 && endAngle >= 360 - ERROR_MARGIN)
			return 1;
		else
			return 0;
	}
	else {
		if (endAngle <= goalAngle + ERROR_MARGIN && endAngle >= goalAngle - ERROR_MARGIN)
			return 1;
		else
			return 0;
	}
}
char* BucketControllerTest::ColourToString(colour c)
{
	switch (c)
	{
	case RED:
		return "Red";
	case GREEN:
		return "Green";
	case BLUE:
		return "Blue";
	case ERROR:
		return "ERROR";
	default:
		return "Default";
	}
}
int BucketControllerTest::FindColourAngle(colour c) {
		switch (c)
	{
	case RED:
		return 0;
	case GREEN:
		return 180;
	case BLUE:
		return 90;
	case ERROR:
		return 270;
	default:
		return 270;
	}
}


