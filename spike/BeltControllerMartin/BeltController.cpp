#include "Motor.h"

#define primFast 80
#define secFast 70
#define primSlow 40
#define secSlow 30

enum beltMode_t { fast, slow };
beltMode_t mode = fast;
bool secRunning = false;
Motor primMotor(Port_A);
Motor secMotor(Port_C);

void setModeFast() 
{
	mode = fast;
	primMotor.setPWM(primFast);
	if (secRunning)
	{
		secMotor.setPWM(secFast);
	}
}

void setModeSlow() 
{
	mode = slow;
	primMotor.setPWM(primSlow);
	if (secRunning)
	{
		secMotor.setPWM(secSlow);
	}
}

void startOrStopSec()
{
	if (secRunning)
	{
		secRunning = false
		secMotor.setPWM(0);
	}
	else
	{
		secRunning = true;
		if (mode == fast)
		{
			setModeFast();
		}
		else
		{
			setModeSlow();
		}
	}
}