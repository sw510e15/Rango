#include "Nxt.h"
#include "Lcd.h"
#include "NxtColorSensor.h"
#include "Speaker.h"
#include "Clock.h"
#include "Motor.h"
#include "queue.cpp"
#include "BucketController.h"

using namespace ecrobot;

extern "C"
{
    /* C Includes and global variables */
    #include "kernel.h"
    #include "kernel_id.h"
    #include "ecrobot_interface.h"
    
    struct LegoBrick{
        int colour;
        int time;
    };
    
    short previousColour = 99;
    short latestValidColour = 99;
    LegoBrick currentBrick;
    
    char *colourMap[8] = {
        "Black", "Blue", "Green", "Yellow", 
        "Orange", "Red", "White", "Unknown"
    };

    /* TOPPERS/ATK declarations */
    DeclareCounter(SysTimerCnt);
    DeclareAlarm(ColourPollingAlarm);
    DeclareAlarm(ColourControllerAlarm);

    /* Initialize NXT devices, ports and utilities */
    Nxt nxt;
    Lcd lcd;
    Clock clock;
    NxtColorSensor colourSensor(PORT_1, NxtColorSensor::_COLORSENSOR);
	Motor motorA(PORT_A);

	/* nxtOSEK hooks */
	void ecrobot_device_initialize(){ ecrobot_init_bt_slave("LEJOS-OSEK"); }
	void ecrobot_device_terminate(){ ecrobot_term_bt_connection(); }

    /* nxtOSEK hook to be invoked from an ISR in category 2 */
    /* Increment OSEK Alarm Counter */
    void user_1ms_isr_type2(void){ (void)SignalCounter(SysTimerCnt); }

    ///=======================================================
    /// TASKS STARTS HERE... 
    ///=======================================================
    /* Poll the colour sensor */
    TASK(ColourPollingTask)
    {
        /* processBackground communicates with colourSensor
        (this must be executed repeatedly in a background Task) */
        colourSensor.processBackground(); 
        TerminateTask();
    }
	
    /* Handle the data recieved from colour sensor */
    TASK(ColourControllerTask)
    {
		motorA.setPWM(50);
        
        //colourSensor.setSensorMode(NxtColorSensor::_COLORSENSOR);
        
        // save current colour and clock.
        S16 rgb[3]; // Array containing the RGB values of the current colour.
        colourSensor.getRawColor(rgb);
        int currentColour = colourSensor.getColorNumber();
        int curClock = clock.now();
        
        // if the colour is orange or black, we ignore it.
        // This is due to sensor is inaccuracy.
        if(currentColour == 4 || currentColour == 0 )
            TerminateTask();
        
        // Clear the LCD buffer
        lcd.clear();
        
        // Print the colour estimated by the colour sensor        
        lcd.cursor(0,0);
        lcd.putf("ss", "CUR_COLOR: ", colourMap[currentColour]);
        
        // Print raw RGB values from colour sensor on a single line
        lcd.cursor(0,1);
        lcd.putf("sd", "R", rgb[0],0);
        lcd.cursor(5,1);
        lcd.putf("sd", "G", rgb[1],0);
        lcd.cursor(10,1);
        lcd.putf("sd", "B", rgb[2],0);
        
        // Print the latest valid colour
        lcd.cursor(0,2);
        lcd.putf("ss", "LVC: ", colourMap[latestValidColour]);
        
        // Display stuff in LCD buffer
        lcd.disp();
        
        //new brick or endidng of the current one.
        if(currentColour != previousColour)
        {
            previousColour = currentColour;
            // making sure the unknown(nr. 99) colour aint enqueued.
            if(currentColour != 99)
            {
                latestValidColour = currentColour;
                LegoBrick brick = {currentColour, curClock};
                currentBrick = brick;
            }
        }
        TerminateTask();
    }
    
    void turnBucket(int colourNr)
    {
        BucketController bct;
        
        colour col = (colour)colourNr;
        bct.TurnBucket(col);
    }
    
    /* Handles bucket motor through bucket controller  */
    TASK(BucketControllerTask)
    {

        Nxt nxt;
        Clock clock;
        turnBucket(currentBrick.colour);

        TerminateTask();
    }
}
