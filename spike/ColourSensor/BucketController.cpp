#include "BucketController.h"
#include "Clock.h"

#include "Lcd.h"
using namespace ecrobot;

Lcd lcd;
Motor motor(PORT_B);
Clock clock;
colour currentColour = RED;
BucketController::BucketController () {}
BucketController::~BucketController() {}
void BucketController::TurnBucket(colour c)
{
	switch (c)
	{
	case RED:
		TurnMotorRed();
		currentColour = RED;
		break;
	case GREEN:
		TurnMotorGreen();
		currentColour = GREEN;
		break;
	case BLUE:
    //error case
	default:
		break;
	}
}



void BucketController::TurnMotorRed()
{
	if (currentColour == RED) 
		return;
	
	motor.setPWM(-50);
	while (motor.getCount() % 360 >= RED_ANGLE + BRAKELENGTH_50PWM) {  }
	motor.setPWM(0);
	clock.wait(150);

	// safety
	BucketSafety(RED_ANGLE);
}
void BucketController::TurnMotorGreen()
{
	if (currentColour == GREEN)
		return;
	
	motor.setPWM(50);
	while (motor.getCount() % 360 <= GREEN_ANGLE - BRAKELENGTH_50PWM) { }
	motor.setPWM(0);
	clock.wait(150);

	// safety
	BucketSafety(GREEN_ANGLE);
}

void BucketController::BucketSafety(int angle) {
	if (motor.getCount() <= angle - 2)
	{
		motor.setPWM(10);
		while (motor.getCount() % 360 <= angle) {}
		motor.setPWM(0);
	}
	else if (motor.getCount() >= angle + 2)
	{
		motor.setPWM(-10);
		while (motor.getCount() % 360 >= angle) {}
		motor.setPWM(0);
	}
}
