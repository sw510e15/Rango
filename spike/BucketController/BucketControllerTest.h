#ifndef BUCKET_CONTROLLER_TEST_H
#define BUCKET_CONTROLLER_TEST

#include "Colour.h"
#include "Motor.h"

class BucketControllerTest {
public:
	BucketControllerTest();
	~BucketControllerTest();
	void BucketTest();
	void BucketRandomTest();
	
private:
	char* colour_to_str(colour_t c);
	int colour_to_angle(colour_t c);
	int FindShortestPath (int curr, int dest);
	inline int positive_modulo(int i, int n);
	void CheckWithinMargin(int t_nr, colour_t c);
};

#endif // BUCKET_CONTROLLER
