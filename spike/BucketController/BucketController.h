#ifndef BUCKET_CONTROLLER_H
#define BUCKET_CONTROLLER
#define UNKNOWN_ANGLE 0
#define RED_ANGLE 90
#define BLUE_ANGLE 180
#define GREEN_ANGLE 270
#define BRAKELENGTH 43
#define ERROR_MARGIN 2
#define BUCKET_MOTOR_PWM 100
#define SAFETY_PWM 10

#include "Colour.h"
#include "Motor.h"

class BucketController {
public:
	BucketController();
	~BucketController();
	void TurnBucket(colour_t c);
private:
	void BucketSafety (int angle);
	void TurnToAngle(int destAngle, colour_t c);
	int FindShortestPath (int curr, int dest);
	int FindDirection (int currentAngle, int destAngle, int shortestPath);
	int FindDirectionSimple (colour_t c);
	inline int positive_modulo(int i, int n);
	const char* colour_to_str(colour_t c);
};

#endif // BUCKET_CONTROLLER
