#include "BucketController.h"
#include "Clock.h"
#include "Lcd.h"

using namespace ecrobot;

Lcd lcd;
Motor motor(PORT_B);
Clock clock;

//Red is the start colour
colour_t currentColour = UNKNOWN;
#pragma region Public functions

BucketController::BucketController () {}
BucketController::~BucketController() {}

//Turns the bucket motor based on a colour
void BucketController::TurnBucket(colour_t c)
{
	// if the current colour is the same as the incomming colour, the bucket should not shift
	if (currentColour == c)
		return;
	
	switch (c)
	{
	case RED:
		TurnToAngle(RED_ANGLE, RED);
		currentColour = RED;
		break;
	case GREEN:
		TurnToAngle(GREEN_ANGLE, GREEN);
		currentColour = GREEN;
		break;
	case BLUE:
		TurnToAngle(BLUE_ANGLE, BLUE);
		currentColour = BLUE;
		break;
	case UNKNOWN:
	default:
		TurnToAngle(UNKNOWN_ANGLE, UNKNOWN);
		currentColour = UNKNOWN;
		break;
	}
}

#pragma endregion 

#pragma region Private functions
//Turns the bucket motor to an given angle
void BucketController::TurnToAngle(int destAngle, colour_t c) {
	// calculate shortest path to destination
	int currentAngle = motor.getCount();
	int shortestPath = FindShortestPath(currentAngle, destAngle);
	// find direction to go // 1 is counterclockwise, -1 is clockwise
	int direction = FindDirection(currentAngle, destAngle, shortestPath);

	// go to destination
	int savedAngle = motor.getCount();
	motor.setPWM(direction * BUCKET_MOTOR_PWM);
	if (direction == 1) {
		while (savedAngle + shortestPath > motor.getCount() + BRAKELENGTH) {}
	} else {
		while (savedAngle - shortestPath < motor.getCount() - BRAKELENGTH) {}
	}
	motor.setPWM(0);
	
	// give the bucket time to settle
	clock.wait(137);
	
	// perform safe check
	BucketSafety(destAngle);
}
//Only for debugging
const char* BucketController::colour_to_str(colour_t c)
{
	switch (c)
	{
	case RED:
		return "Red";
	case GREEN:
		return "Green";
	case BLUE:
		return "Blue";
	case UNKNOWN:
	default:
		return "UNKNOWN";
	}
}
int BucketController::FindDirection (int currentAngle, int destAngle, int shortestPath) {
	if (positive_modulo(currentAngle + shortestPath,360) == destAngle)
		return 1;
	else
		return -1;
}

int BucketController::FindShortestPath (int curr, int dest) {
	int rawDiff = curr > dest ? curr - dest : dest - curr;
	int modDiff = positive_modulo(rawDiff,360);
	return modDiff > 180 ? 360 - modDiff : modDiff;
}
//Inline sugests that the compiler should simply insert this instead of calling the method
inline int BucketController::positive_modulo(int i, int n) {
    return (i % n + n) % n;
}

//Puts the bucket at the rigth posistion if it did not happen on the first try
void BucketController::BucketSafety(int destAngle) {
	int startAngle = motor.getCount();
	int shortestPath = FindShortestPath(startAngle, destAngle);
	int direction = FindDirection(startAngle, destAngle, shortestPath);

	int savedAngle = motor.getCount();
	if (shortestPath > ERROR_MARGIN) {
		motor.setPWM(direction * SAFETY_PWM);
		if (direction == 1) {
			while (savedAngle + shortestPath > motor.getCount()) {}
		} else {
			while (savedAngle - shortestPath < motor.getCount()) {}
		}
		motor.setPWM(0);		
	}
}
#pragma endregion 
