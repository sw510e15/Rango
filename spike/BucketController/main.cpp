// This is the test caller for the BucketController 

// ECRobot++ API
//#include "Motor.h"
#include "Nxt.h"
#include "Clock.h"
#include "Lcd.h"
#include "BucketControllerTest.h"
using namespace ecrobot;

extern "C"
{
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"
using namespace std; // maybe not necessary

void ecrobot_device_initialize()
{
	ecrobot_init_bt_slave("LEJOS-OSEK");
}

void ecrobot_device_terminate()
{
	ecrobot_term_bt_connection();
}

/* nxtOSEK hook to be invoked from an ISR in category 2 */
void user_1ms_isr_type2(void)
{
	SleeperMonitor(); // needed for I2C device and Clock classes
}

TASK(TaskMain)
{
	Nxt nxt;
	Clock clock;
	Lcd lcd;
	
	BucketControllerTest bct;
	bct.BucketTest();
	bct.BucketRandomTest();
	
	lcd.clear();
	lcd.putf("s", "done");
	lcd.disp();
	
	TerminateTask();
}

}



