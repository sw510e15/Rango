// ECRobot++ API
#include "Motor.h"
#include "Nxt.h"
#include "Clock.h"
#include "Lcd.h"
#include "Speaker.h"
#include "BucketController.h"
#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
using namespace ecrobot;

#include "BucketControllerTest.h"

Motor motor(PORT_B);

BucketControllerTest::BucketControllerTest() {}
BucketControllerTest::~BucketControllerTest() {}

void BucketControllerTest::BucketTest()
{
	Nxt nxt;
	Clock clock;
	Lcd lcd;
	
	lcd.clear();
	
	colour_t colours[] = { BLUE,GREEN,UNKNOWN,BLUE,RED,RED,UNKNOWN,BLUE,UNKNOWN,RED,BLUE,RED,GREEN,RED,GREEN,RED,
		UNKNOWN,GREEN,UNKNOWN,GREEN,BLUE,RED,BLUE,BLUE,UNKNOWN,RED,GREEN,GREEN,UNKNOWN,BLUE,RED,RED,GREEN,BLUE,UNKNOWN,RED,GREEN,RED,GREEN,RED,GREEN,RED,
		RED,GREEN,RED,GREEN,UNKNOWN,BLUE,GREEN };
	
	lcd.clear();
	lcd.putf("sns", "Known seq test", "starting...");
	lcd.disp();
	clock.wait(2000);
	
	BucketController bc;
	int length = sizeof(colours) / sizeof(colour_t);
	for (int i = 0; i < length; i++)
	{
		lcd.clear();
		lcd.putf("dssn",i,0," ",colour_to_str(colours[i]));
		lcd.disp();
		bc.TurnBucket(colours[i]);
		
		// check to see if test failed
		CheckWithinMargin(i, colours[i]);
		
		clock.wait(1000);
	}
}


void BucketControllerTest::BucketRandomTest()
{
	Nxt nxt;
	Clock clock;
	Lcd lcd;

	colour_t colours[25];
	int length = sizeof(colours) / sizeof(colour_t);
	srand (clock.now() + 12); // time(null) wrecks it
	
	for (int i = 0; i < length; i++) {
		int enumnr = rand() % 3;
		if (enumnr == 3) 
			enumnr = 99; // UNKNOWN has int value 99
		colours[i] = colour_t(enumnr);
	}
	
	lcd.clear();
	lcd.putf("sns", "Random seq test", "starting...");
	lcd.disp();
	clock.wait(2000);
		
	BucketController bc;
	int i ;
	for ( i = 0; i < length; i++)
	{
		lcd.clear();
		lcd.putf("dssn",i,0," ",colour_to_str(colours[i]));
		lcd.disp();
		bc.TurnBucket(colours[i]);
		
		// allow the bucketsafety to settle
		clock.wait(50);
		
		// check to see if test failed
		CheckWithinMargin(i, colours[i]);
		
		clock.wait(1000);
	}
}

// private
char* BucketControllerTest::colour_to_str(colour_t c)
{
	switch (c)
	{
	case RED:
		return "Red";
	case GREEN:
		return "Green";
	case BLUE:
		return "Blue";
	case UNKNOWN:
		return "UNKNOWN";
	default:
		return "UNKNOWN";
	}
}
int BucketControllerTest::colour_to_angle(colour_t c)
{
	switch (c)
	{
	case RED:
		return RED_ANGLE;
	case GREEN:
		return GREEN_ANGLE;
	case BLUE:
		return BLUE_ANGLE;
	case UNKNOWN:
	default:
		return UNKNOWN_ANGLE;
	}
}
//Inline sugests that the compiler should simply insert this instead of calling the method
inline int BucketControllerTest::positive_modulo(int i, int n) {
    return (i % n + n) % n;
}
void BucketControllerTest::CheckWithinMargin(int t_nr, colour_t c) 
{
	Lcd lcd;
	Speaker speaker;
	Clock clock;
	
	int curr = positive_modulo(motor.getCount(), 360);
	int dest = colour_to_angle(c);
	int sp = FindShortestPath(curr, dest);

	if (sp > ERROR_MARGIN) {
		speaker.playTone(1000, 500, 30);
		lcd.clear();
		lcd.putf("sdnssnsdnsdnsd","t_nr: ",t_nr,0,"col: ",colour_to_str(c),"curr: ",curr,0,"dest: ",dest,0,"sp: ",sp,0);
		lcd.disp();
		clock.wait(10000);
		// TerminateTask();
	}
}
int BucketControllerTest::FindShortestPath (int curr, int dest) 
{
	int rawDiff = curr > dest ? curr - dest : dest - curr;
	int modDiff = rawDiff % 360;
	return modDiff > 180 ? 360 - modDiff : modDiff;
}
