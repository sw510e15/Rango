/* This header file serves make sure the code gets more readable
 * and writeable.
 * USE WITH CARE
 * //Rene
*/
#ifndef Colour_h
#define Colour_H
#include "NxtColorSensor.h"

typedef  ecrobot::NxtColorSensor::eColorNumber colour_t;

#define BLACK ecrobot::NxtColorSensor::_BLACK 
#define BLUE ecrobot::NxtColorSensor::_BLUE 
#define GREEN ecrobot::NxtColorSensor::_GREEN 
#define YELLOW ecrobot::NxtColorSensor::_YELLOW 
#define ORANGE ecrobot::NxtColorSensor::_ORANGE 
#define RED  ecrobot::NxtColorSensor::_RED 
#define	WHITE ecrobot::NxtColorSensor::_WHITE 
#define UNKNOWN ecrobot::NxtColorSensor::_UNKNOWN

#endif
