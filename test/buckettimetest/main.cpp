#include "Nxt.h"
#include "Lcd.h"
#include "Speaker.h"
#include "Clock.h"
#include "Motor.h"
#include "Bluetooth.h"
#include "Daq.h"

#include "Colour.h"
#include "BucketController.h"

#include <string.h>

using namespace ecrobot;

extern "C"
{
    /* C Includes and global variables */
	#include "kernel.h"
	#include "kernel_id.h"
	#include "ecrobot_interface.h"

    BucketController bucketController;

    /* TOPPERS/ATK declarations */
    DeclareCounter(SysTimerCnt);

    /* Initialize NXT devices, ports and utilities */
    Clock clock;
	Lcd lcd;
	Nxt nxt;
	Bluetooth bt;
	Daq daq(bt);

    /* nxtOSEK hooks */
    void ecrobot_device_initialize(){ ecrobot_init_bt_slave("LEJOS-OSEK"); }
    void ecrobot_device_terminate() { ecrobot_term_bt_connection(); }

    /* nxtOSEK hook to be invoked from an ISR in category 2 */
    /* Increment OSEK Alarm Counter */
    void user_1ms_isr_type2(void) { (void)SignalCounter(SysTimerCnt); }

    ///=======================================================
    /// TASKS STARTS HERE... 
    ///=======================================================

    /* Starting task. Runs only once. */
    TASK(InitializationTask)
    {
        lcd.clear();
		bt.waitForConnection("1234",500);
        TerminateTask();
    }

    TASK(BucketTurnTask)
    {
        //currentTask = 2;
        bucketController.turnBucket();
        TerminateTask();
    }

	unsigned int bucketTurnTime = 0;
	unsigned int i = 0;
	// colour_t colArr[] = {BLUE,WHITE,BLUE,WHITE,BLUE,WHITE,BLUE,WHITE,BLUE,WHITE,BLUE,WHITE,BLUE,WHITE,BLUE,WHITE,BLUE,WHITE,BLUE,WHITE}; // 360 deg test
	colour_t colArr[] = {RED,BLUE,GREEN,WHITE,RED,BLUE,GREEN,WHITE,RED,BLUE,GREEN,WHITE,RED,BLUE,GREEN,WHITE,RED,BLUE,GREEN,WHITE}; // 90 deg test
	unsigned int arrLen = sizeof(colArr)/sizeof(colour_t);
	
    /* Handles bucket motor through bucket controller  */
    TASK(BucketControllerTask)
    {
		if (clock.now() > bucketTurnTime)
		{
			bucketTurnTime += 2000;
			
			if (i < arrLen)
			{
				lcd.clear();
				lcd.putf("d",colArr[i],0);
				lcd.disp();
				
				bucketController.startTurn(colArr[i++]);
			}
			else
			{
				lcd.clear();
				lcd.putf("s","done");
				lcd.disp();				
			}
		}
		
		TerminateTask();
    }

    /* Update LCD display for debugging */
	// Alt det gamle LCD kode er blevet smidt i oldLCDCode.txt
    TASK(ShowDisplayTask)
    {
        //currentTask = 5;
        lcd.clear();

		lcd.disp();
		
        TerminateTask();
    }
}
