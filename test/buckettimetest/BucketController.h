#pragma once
#ifndef BUCKET_CONTROLLER_H
#define BUCKET_CONTROLLER
#define UNKNOWN_ANGLE 0
#define RED_ANGLE 90
#define BLUE_ANGLE 180
#define GREEN_ANGLE 270
#define BRAKELENGTH 43
#define ERROR_MARGIN 3
#define MOTOR_PWM 100
#define CORRECTION_PWM 10 
#define WAIT_TIME 150

#include "Colour.h"
#include "Motor.h"
#include "Clock.h"

#include "BucketEnums.h"

class BucketController {
public:
	BucketController();
	~BucketController();
	void startTurn(colour_t c);
	void turnBucket();
	void calcLengthAndDirection(const int nextAngle, int motorPWM, int brakeLength);
private:
    Bucketstate_t state;
	
	void datalog();
    void isMotorSettled();
	void resetTurnData();
	void hasTravelledDistance();
	inline void tweakAngle(int destAngle);	
	//void turnToAngle(int destAngle);
	//void turnToAngle(int destAngle,int motorPWM,int brakeLenght);
	int findShortestDistance (int curr, int dest);
	Direction_t findDirection (int currentAngle, int destAngle, int shortestPath);
	int getAngle(colour_t colour);
	void startCorrectionTurn();
	bool correctionTurnDone();
	inline int positive_modulo(int i, int n);
	const char* colourToStr(colour_t c);
};                                     

#endif // BUCKET_CONTROLLER
