/*
    This is the color recognition test for the Colour sensor 3 eyes    todo: find right name

    The NXT Charting Logger is used to receive data from the nxt, so it must be opened, the connection
    must be usb.




*/

import lejos.nxt.*;
import lejos.nxt.ColorSensor;
import lejos.nxt.SensorPort;
import lejos.nxt.comm.NXTConnection;
import lejos.nxt.comm.USB;
import lejos.util.LogColumn;
import lejos.util.NXTDataLogger;

import java.io.*;
import java.lang.InterruptedException;


public class Main {
    public static void main (String[] args) {
        System.out.println("Initializing data logger");

        //Initialize the Data logger which is needed
        NXTDataLogger dlog = new NXTDataLogger();

        ColorSensor colorSensor = new ColorSensor(SensorPort.S1);

        //I have tried to lookup the com port for the bluetooth device (found in bluetooth settings)
        System.out.println("Waiting for USB");

        //Setups the USB connection (BT can also be used
        NXTConnection connection = USB.waitForConnection();

        System.out.println("USB Connected");
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

        try {
            //the dlog is started at default on initialization but we now it has to log at Realtime
            dlog.startRealtimeLog(connection);
        } catch (IOException e) {
            // Do nothing. This is hideously bad. //Comment from the devs of the example i used
        }
        //Sets the colums og the log
        dlog.setColumns(new LogColumn[] {
                //If more is needed then add new LogColum more
                new LogColumn("Colour Id: ", LogColumn.DT_INTEGER),
                new LogColumn("red: ", LogColumn.DT_INTEGER),
                new LogColumn("green: ", LogColumn.DT_INTEGER),
                new LogColumn("blue: ", LogColumn.DT_INTEGER)
        });

        for(int i = 0 ; i < 100 ; i++) {

            //Writes the distance to the log
            dlog.writeLog(colorSensor.getColorID());
            ColorSensor.Color color = colorSensor.getColor();
            dlog.writeComment(findColour(color));
            dlog.writeLog(color.getRed());
            dlog.writeLog(color.getGreen());
            dlog.writeLog(color.getBlue());

            System.out.print(findColourSimple(color));

            //Finish the line in the log (Have to do this when you are done with a line
            dlog.finishLine();
            try {
                Thread.sleep(200);
            }
            catch (InterruptedException e) {
                // lol do nothing
            }
        }

        dlog.stopLogging();
        connection.close();

        System.out.println("-");
        Button.waitForAnyPress();
    }

    private static String findColour(ColorSensor.Color colour) {
        switch (colour.getColor()){
            case 0:
                return "Red";
            case 1:
                return "Green";
            case 2:
                return "Blue";
            case 3:
                return "Yellow";
            case 4:
                return "Magenta";
            case 5:
                return "Orange";
            case 6:
                return "White";
            case 7:
                return "Black";
            case 8:
                return "Pink";
            case 9:
                return "Gray";
            case 10:
                return "Light Gray";
            case 11:
                return "Dark Gray";
            case 12:
                return "Cyan";
            default:
                return "Error";
        }
    }

    private static String findColourSimple(ColorSensor.Color colour) {
        switch (colour.getColor()){
            case 0:
                return "r";
            case 1:
                return "g";
            case 2:
                return "b";
            case 3:
                return "y";
            case 4:
                return "m";
            case 5:
                return "o";
            case 6:
                return "w";
            case 7:
                return "B";
            case 8:
                return "p";
            case 9:
                return "G";
            case 10:
                return "-LG-";
            case 11:
                return "-DG-";
            case 12:
                return "c";
            default:
                return "e";
        }
    }
}
