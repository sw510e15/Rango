import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.ButtonListener;

public class SimpleMotorRun {
    public static class MotorInfo {
        public static Integer power = 0;
        public static Integer powerchange = 100;
    }

    public static void main (String[] args) throws InterruptedException{
        System.out.println("Ready to go");
        Button.LEFT.addButtonListener(new LeftButtonListener(Button.LEFT));
        Button.RIGHT.addButtonListener(new RightButtonListener(Button.RIGHT));
        Button.ENTER.addButtonListener(new MiddleButtonListener(Button.ENTER));
        Button.ESCAPE.waitForPress();
    }

    public static class LeftButtonListener implements ButtonListener {
        Button b;
        LeftButtonListener(Button btn) {
            b = btn;
        }

        @Override
        public void buttonPressed(Button button) {
            MotorInfo.power -= MotorInfo.powerchange;
            Motor.A.setSpeed(MotorInfo.power);
            if (MotorInfo.power < 0) {
                Motor.A.forward();
                System.out.println("Power counterclockwise: " + MotorInfo.power);
            }
            else {
                Motor.A.backward();
                System.out.println("Power clockwise: " + MotorInfo.power);
            }
        }
        @Override
        public void buttonReleased(Button button){
        }
        @Override
        public void finalize() {}
    }
    public static class RightButtonListener implements ButtonListener {
        Button b;
        RightButtonListener(Button btn) {
            b = btn;
        }

        @Override
        public void buttonPressed(Button button) {
            MotorInfo.power += MotorInfo.powerchange;
            Motor.A.setSpeed(MotorInfo.power);
            if (MotorInfo.power < 0) {
                Motor.A.forward();
                System.out.println("Power counterclockwise: " + MotorInfo.power);
            }
            else {
                Motor.A.backward();
                System.out.println("Power clockwise: " + MotorInfo.power);
            }
        }
        @Override
        public void buttonReleased(Button button){
        }
        @Override
        public void finalize() {}
    }
    public static class MiddleButtonListener implements ButtonListener {
        Button b;
        MiddleButtonListener(Button btn) {
            b = btn;
        }

        @Override
        public void buttonPressed(Button button) {

            if (MotorInfo.powerchange == 100)
                MotorInfo.powerchange = 10;
            else if (MotorInfo.powerchange == 10)
                MotorInfo.powerchange = 20;
            else if (MotorInfo.powerchange == 20)
                MotorInfo.powerchange = 50;
            else if (MotorInfo.powerchange == 50)
                MotorInfo.powerchange = 100;

            System.out.println("Power change changed: " + MotorInfo.powerchange);
            MotorInfo.power = 0;
            Motor.A.flt();
        }
        @Override
        public void buttonReleased(Button button){
        }
        @Override
        public void finalize() {}
    }
}

