/*
  This is a example of how the Data logger is used
  Here usb connection is used (a BT connection can be used if needed)

  http://www.lejos.org/nxt/nxj/tutorial/Utilities/LejosUtilities.htm#nxtdl
  The NXTDataLogger requires the NXT charting logger to be open and connected to the nxt


  Made By Rene Mejer 21-09-2015
*/

import lejos.nxt.*;
import lejos.nxt.Button;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;
import lejos.util.LogColumn;
import lejos.util.NXTDataLogger;

import java.io.*;


public class Logging {

    public static void main (String[] args) {
        System.out.println("Logging");

        //Initialize the Data logger which is needed
        NXTDataLogger dlog = new NXTDataLogger();

        ColorSensor sensor = new ColorSensor(SensorPort.S1);

        //I have tried to lookup the com port for the bluetooth device (found in bluetooth settings)
        System.out.println("Waiting for Connection");

        //Setups the USB connection (BT can also be used
        NXTConnection connection = Bluetooth.waitForConnection();

        System.out.println("USB Connected");

        try {
            //the dlog is started at default on initialization but we now it has to log at Realtime
            dlog.startRealtimeLog(connection);
        } catch (IOException e) {
            // Do nothing. This is hideously bad. //Comment from the devs of the example i used
        }
        System.out.println("Press Right for \nSimpleColour \nOr Left for \nRGBColour");
        boolean rightButtonPressed = false;

        while(!rightButtonPressed) {
            if (Button.RIGHT.isDown()) {
                SimpleColour(dlog, sensor);
                rightButtonPressed = true;
            }else if (Button.LEFT.isDown()) {
                RGBColour(dlog, sensor);
                rightButtonPressed = true;
            }
        }
        dlog.stopLogging();

        connection.close();

        System.out.println("Done :)");
        Button.waitForAnyPress();
    }

    private static void RGBColour(NXTDataLogger dlog, ColorSensor sensor) {
        dlog.setColumns(new LogColumn[]{
            new LogColumn("Red ", LogColumn.DT_INTEGER),
            new LogColumn("Green", LogColumn.DT_INTEGER),
            new LogColumn("Blue", LogColumn.DT_INTEGER)
        });
        String message;
        ColorSensor.Color colour;
        LCD.clear();
        for(int i=0; i < 5000; i++){
            colour = sensor.getColor();
            message = "Red: " +colour.getRed() + "\n \n"+
                      "Green: " + colour.getGreen()+ "\n \n"+
                      "Blue: " + colour.getBlue() + "\n";
            dlog.writeLog(colour.getRed());
            dlog.writeLog(colour.getGreen());
            dlog.writeLog(colour.getBlue());
            //Makes the data stand out nicely
            LCD.drawString(message, 0, 0, false);
            //System.out.println(message);

            dlog.finishLine();
        }
    }

    private static void SimpleColour(NXTDataLogger dlog, ColorSensor sensor) {
        //Sets the colums og the log
        dlog.setColumns(new LogColumn[] {
                //If more is needed then add new LogColum more
                new LogColumn("Colour: ", LogColumn.DT_INTEGER)
        });

        String message;

        for(int i = 0 ; true ;i++) {
                ColorSensor.Color colour = sensor.getColor();
                String colourText = findColour(colour);

                message = colourText + " Nr: "+ colour.getColor();
                System.out.println(message);
                //Writes the distance to the log
                dlog.writeLog(colour.getColor());
                dlog.writeComment(colourText);
                //Finish the line in the log (Have to do this when you are done with a line
                dlog.finishLine();
        }
    }


    private static String findColour(ColorSensor.Color colour) {
        switch (colour.getColor()){
            case 0:
                return "Red";
            case 1:
                return "Green";
            case 2:
                return "Blue";
            case 3:
                return "Yellow";
            case 4:
                return "Magenta";
            case 5:
                return "Orange";
            case 6:
                return "White";
            case 7:
                return "Black";
            case 8:
                return "Pink";
            case 9:
                return "Gray";
            case 10:
                return "Light Gray";
            case 11:
                return "Dark Gray";
            case 12:
                return "Cyan";
            default:
                return "Error";
        }
    }
}
