import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.Sound;
import lejos.nxt.UltrasonicSensor;

public class LeftButton implements ButtonListener{

    private UltrasonicSensor ultrasonicSensor;

    public LeftButton (UltrasonicSensor usc){
        this.ultrasonicSensor = usc;
    }

    @Override
    public void buttonPressed(Button button) {
        if(ultrasonicSensor.getMode() == 0){
            ultrasonicSensor.continuous();
            Sound.buzz();
        }
        else if(ultrasonicSensor.getMode() == 2){
            ultrasonicSensor.off();
            Sound.twoBeeps();
        }
    }

    @Override
    public void buttonReleased(Button button) {

    }
}
