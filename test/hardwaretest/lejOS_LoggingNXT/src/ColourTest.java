import lejos.nxt.Button;
import lejos.nxt.ColorSensor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;
import lejos.util.Delay;
import lejos.util.LogColumn;
import lejos.util.NXTDataLogger;

import java.io.IOException;

public class ColourTest {
    public static void main (String[] args) {

        NXTDataLogger dlog = new NXTDataLogger();

        ColorSensor sensor = new ColorSensor(SensorPort.S1);

        System.out.println("Waiting for PC");

        NXTConnection connection = Bluetooth.waitForConnection();
        Sound.beep();
        System.out.println("Connected");

        try {
            dlog.startRealtimeLog(connection);
        } catch (IOException e) {
            System.exit(-1);
        }

        dlog.setColumns(new LogColumn[]{
                new LogColumn("Colour: ", LogColumn.DT_INTEGER)
        });

        //boolean exitButtonPressed = false;
        int i = 0;
        while (i < 25){
           // if (Button.ESCAPE.isDown())
              //  exitButtonPressed = true;
            //else
                SimpleColour(dlog,sensor);
            i++;
        }

        dlog.stopLogging();
        connection.close();
        //Button.waitForAnyPress();
    }


    private static void SimpleColour(NXTDataLogger dlog, ColorSensor sensor) {
        String message;

        ColorSensor.Color colour = sensor.getColor();
        String colourText = findColour(colour);

        message = colourText + " Nr: "+ colour.getColor();
        System.out.println(message);

        dlog.writeLog(colour.getColor());

        dlog.finishLine();
        Delay.msDelay(50);
    }

    private static String findColour(ColorSensor.Color colour) {
        switch (colour.getColor()){
            case 0:
                return "Red";
            case 1:
                return "Green";
            case 2:
                return "Blue";
            case 3:
                return "Yellow";
            case 4:
                return "Magenta";
            case 5:
                return "Orange";
            case 6:
                return "White";
            case 7:
                return "Black";
            case 8:
                return "Pink";
            case 9:
                return "Gray";
            case 10:
                return "Light Gray";
            case 11:
                return "Dark Gray";
            case 12:
                return "Cyan";
            default:
                return "Error";
        }
    }
}
