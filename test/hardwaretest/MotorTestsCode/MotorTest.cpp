/* sample.cpp for TOPPERS/ATK(OSEK) */ 

// ECRobot++ API
#include "Motor.h"
#include "Nxt.h"
#include "Clock.h"
#include "Lcd.h"
using namespace ecrobot;

extern "C"
{
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"

void GoAroundXDeg (Clock clock, Lcd lcd, int deg, int pow);
void goBackAndForth(Lcd lcd, int deg, int pow);
void getCountPrecisionTest (Clock clock, Lcd lcd, int deg);
void CombinationTest360Deg(Clock clock, Lcd lcd);
void RPMTest(Clock clock, Lcd lcd);

void ecrobot_device_initialize()
{
	ecrobot_init_bt_slave("LEJOS-OSEK");
}

void ecrobot_device_terminate()
{
	ecrobot_term_bt_connection();
}


Motor motorA(PORT_A); // brake by defalut
Motor motorB(PORT_B); // brake by defalut
Motor motorC(PORT_C); // brake by defalut 

/* nxtOSEK hook to be invoked from an ISR in category 2 */
void user_1ms_isr_type2(void)
{
	SleeperMonitor(); // needed for I2C device and Clock classes
}

TASK(TaskMain)
{
	Nxt nxt;
	Clock clock;
	Lcd lcd;
	
	RPMTest(clock, lcd);

	// getCountPrecisionTest(clock, lcd, 90);
	
	// while(1) {
		// if (ecrobot_is_ENTER_button_pressed() == 1) {
			// GoAroundXDeg(clock, lcd, 360 * 20, 50);
		// }
		// clock.wait(2);
	// }
	
	TerminateTask();
}

void RPMTest(Clock clock, Lcd lcd) {
	int pow = 10;
	int testnr = 0;
	
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 1; j++) {
			pow = (i+1)*10;
			
			lcd.clear();
			lcd.putf("d", testnr++);
			lcd.disp();
			
			clock.reset();
			motorA.reset();
			
			motorA.setPWM(pow);
			clock.wait(30 * 1000);
			
			ecrobot_bt_data_logger(pow, motorA.getCount() / 180);
			
			motorA.reset();
			clock.wait(2000);
		}
	}
}

void CombinationTest360Deg(Clock clock, Lcd lcd) {
	int testcount = 1;
	int pow = 10;

	while(1) {
		if (ecrobot_is_ENTER_button_pressed() == 1) {
			GoAroundXDeg(clock, lcd, 360, pow);
			
			if (testcount == 10) {
				testcount = 0;
				
				if (pow == 10)
					pow = 30;
				else if (pow == 30)
					pow = 50;
				else if (pow == 50)
					pow = 70;
				else
					pow = 100;
			}
			testcount++;
		}
		clock.wait(2);
	}
}

void GoAroundXDeg (Clock clock, Lcd lcd, int deg, int pow) {
	motorA.reset();
	motorA.setPWM(pow);
	while(1)
	{
		if (motorA.getCount() >= deg) {
			motorA.reset(); // try it later
			break;
		}
	}
	clock.wait(2000);
	ecrobot_bt_data_logger(pow, motorA.getCount());
	lcd.clear();
	lcd.putf("d", motorA.getCount());
	lcd.disp();
	motorA.setCount(0);
}
void goBackAndForth (Lcd lcd, int deg, int pow) {
	motorA.setPWM(pow);
	while(1)
	{
		if (motorA.getCount() >= deg) {
			motorA.setPWM(-pow);
			break;
			// motorA.reset(); // try it later
		}
	}
	while(1)
	{
		if (motorA.getCount() <= 0) {
			motorA.setPWM(0);
			break;
			// motorA.reset(); // try it later
		}
	}
}

void getCountPrecisionTest (Clock clock, Lcd lcd, int deg) {
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++) {
			motorA.reset();
			motorB.reset();
			int pow = (i + 1) * 10;
			
			motorA.setPWM(pow);
			while(1)
			{
				if (motorA.getCount() >= deg) {
					motorA.reset();
					break;
				}
			}
			
			motorB.setPWM(pow);
			while(1)
			{
				if (motorB.getCount() >= deg) {
					motorB.reset();
					break;
				}
			}
			
			clock.wait(2000);
			ecrobot_bt_data_logger(pow, 0);
		}
	}
}

}



