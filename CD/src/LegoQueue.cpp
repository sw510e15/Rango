#include "LegoQueue.h"
#include "Colour.h"
#include <string.h>

namespace queue
{
    namespace
    {
        struct node
        {
            LegoBrick data;
            node* next;
			node* prev;
            node(const LegoBrick brick, node* prev)
				: data(brick), next(0), prev(prev) {}
        };
        
        node* head = 0;
        node* tail = 0;
        int length = 0;
        
        inline void drop()
        {
            node* n = head;
			
			if (n->next != 0)
			{
				n->next->prev = 0;
			}
            
			if (length == 1) // reset head and tail
			{
				head = 0;
				tail = 0;
			}
			else 
			{
				head = head->next;
			}
            
			delete n;
        }
    }

    void enqueue(const LegoBrick brick)
    {
        node*& next = head ? tail->next : head;
		next = new node(brick, tail);
        tail = next;
        length++;
    }
    
    void enqueue(const colour_t colour, const int startRPM)
    {
        LegoBrick newBrick(colour, startRPM);
        enqueue(newBrick);
		// check if the newly enqueued brick is too close to the next brick
		closenessCheck();
    }
    
    LegoBrick dequeue()
    {
        LegoBrick tmp = head->data;
        
        if(!isEmpty()) 
		{
            drop();
            length--;
        }
        
        return tmp;
    }
    
    bool isEmpty()
    {
        return head == 0;
    }
    
    int getLength()
    {
        return length;
    }
    
    LegoBrick peek()
    {
        return head->data;
    }
    
    LegoBrick peekNext()
    {
        return head->next->data;
    }
	
	void closenessCheck()
	{
		if (length > 1)
		{
			int dist = tail->data.startRevol - tail->prev->data.startRevol;
			// The belt must go at slow speed for this
			if (dist < BRICKS_SLOW_CLOSE && tail->prev->data.colour != tail->data.colour)
			{
				tail->data.slow = true;

			}
			// The belt must go at medium speed for this
			else if(dist < BRICKS_MEDIUM_CLOSE && tail->prev->data.colour != tail->data.colour)
			{
				tail->data.medium = true; 
			}
		}
	}

	void bricksInQueueString(char* str)
	{
		if (!isEmpty()) 
		{
			node* n = head;
			while (1)
			{
				strcat(str, colour_to_str(n->data.colour));
				strcat(str, " ");
				
				if (n->data.slow) {
					strcat(str, "Slow");
				} else if (n->data.medium) {
					strcat(str, "Medium");
				} else {
					strcat(str, "Fast");
				}
				strcat(str, "\n");
				
				if (n->next != 0)
					n = n->next;
				else 
					break;	
			}			
		}
		else 
		{
			strcpy(str, "");
		}
	}
	
	
	
	const char* colour_to_str(colour_t c)
	{
		switch (c)
		{
		case RED:
			return "Red";
		case GREEN:
			return "Green";
		case BLUE:
			return "Blue";
		case YELLOW:
			return "Yellow(U)";
		case WHITE:
			return "White(U)";
		case UNKNOWN:
			return "UNKNOWN";
		case ORANGE:
			return "Orange(U)";
		case BLACK:
			return "Black(U)";
		default:
			return "UNKNOWN";
		}
	}
}

