#pragma once
#ifndef BUCKET_ENUM    
#define BUCKET_ENUM    

enum direction_t { CLOCKWISE = -1, COUNTERCLOCKWISE = 1 };
enum bucketstate_t { STANDBY, SETTLING, TURNING, TWEAKING };

#endif
