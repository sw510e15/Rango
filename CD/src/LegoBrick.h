#pragma once
#ifndef LEGOBRICK_H
#define LEGOBRICK_H
#define REVOLS_TO_DROP 1600

#include "Colour.h"

struct LegoBrick {
	colour_t colour;
	int startRevol;
	int dropRevol;
	bool slow;
	bool medium;
	LegoBrick(const colour_t colour, const int startRevol) 
		: colour(colour), startRevol(startRevol), 
		  dropRevol(startRevol + REVOLS_TO_DROP), 
		  slow(false), medium(false){}
};
#endif
