#pragma once
#ifndef BELT_CONTROLLER_H
#define BELT_CONTROLLER_H
#define	MOTOR_SPEED_FAST 85
#define	MOTOR_SPEED_MEDIUM 45
#define	MOTOR_SPEED_SLOW 27

#include "Motor.h"

enum beltMode_t { FAST, MEDIUM, SLOW };

class BeltController
{
public:
	BeltController();
	~BeltController();
    
	void setSpeedFast();
	void setSpeedMedium();
	void setSpeedSlow();
    void startSecondMotor();
	void stopSecondMotor();
	void startMotors(bool startSecond);
	bool isSecondMotorRunning();
	S32 getMotorCount();
    
private:
    void updateMotors();
	void startPrimeMotor();
};
#endif 
