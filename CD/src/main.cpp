#include "Nxt.h"
#include "Lcd.h"
#include "Clock.h"
#include "Motor.h"
#include "LegoBrick.h"
#include "Colour.h"
#include "BucketController.h"
#include "ColourController.h"
#include "BeltController.h"
#include "LegoQueue.h"
#include "TouchSensor.h"
#include <string.h>

#define SLOW_PREDROP_START_REVOL 140
#define ENDZONE_TURN_REVOLUTIONS 500


using namespace ecrobot;

extern "C"
{
    /* C Includes and global variables */
	#include "kernel.h"
	#include "kernel_id.h"
	#include "ecrobot_interface.h"

    // Prototypes
    void changeSpeed();

    ColourController colourController;
    BucketController bucketController;
    BeltController beltController;

    /* TOPPERS/ATK declarations */
    DeclareCounter(SysTimerCnt);
    DeclareResource(Queue);
    DeclareAlarm(ColourSamplingAlarm);
    DeclareAlarm(ColourControllerAlarm);

    /* NXT devices, ports and utilities */
    Clock clock;
	Lcd lcd;
	Nxt nxt;
    TouchSensor touchSensor(PORT_4);
    
	int* brickFirstSeen; // used to record the first time a brick is seen by the colour sensor

    /* nxtOSEK hooks */
    void ecrobot_device_initialize(){ ecrobot_init_bt_slave("LEJOS-OSEK"); }
    void ecrobot_device_terminate() { ecrobot_term_bt_connection(); }

    /* Increment OSEK Alarm Counter */
    void user_1ms_isr_type2(void) { (void)SignalCounter(SysTimerCnt); }
    

    ///====================
    /// TASKS
    ///====================

    /* Starting task. Runs only once. */
    TASK(InitializationTask)
    {
        lcd.clear();
		*brickFirstSeen = 0;
		
        // Initialize the colour sensor with values from process background
        colourController.processBackround();
        
        // Initialize and start the belt motor.
        beltController.startMotors(false);

        TerminateTask();
    }

    /* Sample the colour sensor */
    TASK(ColourSamplingTask)
    {
        
        /* ProcessBackground communicates with colourSensor
           (this must be executed repeatedly in a background Task) */
        colourController.processBackround();
        
        TerminateTask();
    } 


    /* Handle the data recieved from colour sensor */
    TASK(ColourControllerTask)
    {	
        
		colour_t observedColour = colourController.getObservedColour();
		
		if(colourController.hasSeenNewBrick(observedColour, beltController.getMotorCount(), brickFirstSeen))
		{
			GetResource(Queue);

			queue::enqueue(colourController.getBrickColour(), *brickFirstSeen);
			queue::closenessCheck();

			ReleaseResource(Queue);
		}
        
        TerminateTask();
    }
    
    TASK(BeltControllerTask)
    {
        GetResource(Queue);

        if(!queue::isEmpty())
        {
            // Drop brick if at dropzone
            if(queue::peek().dropRevol < beltController.getMotorCount())
            {
                queue::dequeue();
            }
        }
		// See if speed should be changed
		changeSpeed();

        ReleaseResource(Queue);

        TerminateTask();
    }
    
    TASK(BucketTurnTask)
    {
        
        bucketController.turnBucket();
        
        TerminateTask();
    }

    /* Handles bucket motor through bucket controller  */
    TASK(BucketControllerTask)
    {
        GetResource(Queue);
        
        if(!queue::isEmpty())
        {
            LegoBrick brick = queue::peek();

            if (brick.dropRevol - beltController.getMotorCount() < ENDZONE_TURN_REVOLUTIONS)
            {
                bucketController.startTurn(brick.colour);
            }
            else
            {
                bucketController.startTurn(UNKNOWN);
            }
        }
        else 
        {
            bucketController.startTurn(UNKNOWN);
        }
        
        ReleaseResource(Queue);

        TerminateTask();
    }

    

    /* Update LCD display for debugging */
    TASK(ShowDisplayTask)
    {
        lcd.clear();
	    char strbuf [100];
		
		GetResource(Queue);
	    queue::bricksInQueueString(strbuf);
		ReleaseResource(Queue);
		
		lcd.putf("s",strbuf);
	    strcpy(strbuf, "");        

		lcd.disp();
		
        TerminateTask();
    }

    TASK(NxtButtonTask)
    {
		//Check if the orange button or touch sensor is pressed
		if(nxt.getButtons() & Nxt::ENTR_ON || touchSensor.isPressed())
		{
			beltController.isSecondMotorRunning() ? beltController.stopSecondMotor() : 
                                                    beltController.startSecondMotor();
		}
		TerminateTask();
    }
    
    

    ///==============
    /// FUNCTIONS
    ///==============
	
	void changeSpeed()
	{
		if (!queue::isEmpty())
		{
			LegoBrick head = queue::peek();
			bool brickCloseToDrop = head.dropRevol - beltController.getMotorCount() < SLOW_PREDROP_START_REVOL;
		
			// If there is less than SLOW_PREDROP_START_REVOL motor degrees untill a brick drops,
			// check to see if next brick is slow.		
			if (head.slow == true || (queue::getLength() > 1 && brickCloseToDrop && queue::peekNext().slow == true))
			{
				beltController.setSpeedSlow();
				return;
			}
			else if (head.medium == true || (queue::getLength() > 1 && brickCloseToDrop && queue::peekNext().medium == true))
			{
				beltController.setSpeedMedium();
				return;
			}
		}
        
		beltController.setSpeedFast();
	}
}
