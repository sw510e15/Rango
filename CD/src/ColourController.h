#pragma once
#ifndef COLOUR_CONTROLLER_H
#define COLOUR_CONTROLLER_H
#define COLOUR_COUNT_REDUNDANCY 6 // How many times the colour sensor have to read a certain
                                  // colour in order to believe its input is correct.
#define BRICK_REV_LENGTH 270

#include "NxtColorSensor.h"
#include "Colour.h"
#include "LegoQueue.h"

class ColourController
{
public:
	ColourController();
	~ColourController();
    
    colour_t getObservedColour();
    colour_t getBrickColour();
    bool hasSeenNewBrick(colour_t colour, const int motorCount, int* brickFirstSeen);
    void processBackround();
private:
    bool isAcceptedColour(colour_t colour);
};

#endif // COLOUR_CONTROLLER
