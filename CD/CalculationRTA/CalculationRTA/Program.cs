﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculationRTA
{
    public class task
    {
        public int periode, priority;
        public double WCET, criticalBlockingTime;
        public bool blocking, usage;
        public string name;

        public task(string name, int periode, double WCET, int priority, bool blocking, double criticalBlockingTime)
        {
            this.name = name;
            this.periode = periode;
            this.WCET = WCET;
            this.priority = priority;
            this.blocking = blocking;
            this.criticalBlockingTime = criticalBlockingTime;
            this.usage = false;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<task> tasks = new List<task>();

            //init
            tasks.Add(new task("ColourSmpl", 8000, 2773, 7, false, 2773));
            tasks.Add(new task("ColourCtr", 10000, 279, 6, true, 279));
            tasks.Add(new task("BeltCtr", 10000, 209, 5, true, 209));
            tasks.Add(new task("BucketTurn", 10000, 183, 4, false, 183));
            tasks.Add(new task("BucketCtr", 50000, 199, 3, true, 199));
            tasks.Add(new task("Display", 500000, 1005, 2, true, 1005));
            tasks.Add(new task("Button", 1000000, 183, 1, false, 183));

            //Get list of worst-case response time for each task
            List<double> test = calculateRTA(tasks);

            // print out the result
            Console.WriteLine("----------------------------------------------------------------------------");
            Console.WriteLine("   Tasks   |" + " Period(T)  |" + " Priority(P) |" + " WCET(c) |" + " Mutex(S) |"+  " Usage |"  + "   R  |" );
            Console.WriteLine("----------------------------------------------------------------------------");
            for (int i = 0; i < test.Count; i++)
            {
                Console.WriteLine(String.Format("{0,-10} |   {1,7}  |   {2,4}      |  {3,4}   |   {4,4}  |{5,4}   | {6,4} |", 
                                  tasks[i].name, tasks[i].periode, tasks[i].priority, tasks[i].WCET, tasks[i].blocking ? "{ 1 }" :  "{   }", 
                                  tasks[i].usage ? 1 : 0, test[i]));
            }
            Console.WriteLine("----------------------------------------------------------------------------");
            Console.Read();
        }
 
        // Calculate RTA with interference and blocking, on a list sorted by priority(higster first).
        // Only works with a single resoruce semaphore.
        public static List<double> calculateRTA(List<task> tasks)
        {
            List<double> RTA = new List<double>();
            
            for (int i = 0; i < tasks.Count; i++)
            {
                double R_i = 0, R_i1 = 0;
                task taskI = tasks[i];

                R_i = taskI.WCET;
                //The recursion step until R_i1 == R_i
                while (true)
                {
                    R_i1 = taskI.WCET + calcInterference(tasks, R_i, i) + calcBlocking(tasks, i);
                    if (R_i1 == R_i)
                    {
                        //Case a WCET of task is 0.
                        if (R_i1 == 0)
                            R_i1 = RTA[RTA.Count - 1];

                        RTA.Add(R_i1);
                        break;
                    }
                    R_i = R_i1;
                }
            }
            return RTA;
        }

        public static double calcInterference(List<task> tasks, double R_i, int index)
        {
            double result = 0;
            // Highest priority task, dont have inference from other tasks
            if (tasks[index].priority == tasks.Count)
                return result;

            List<task> hp_i = tasks.GetRange(0, index);
            for (int i = 0; i < hp_i.Count; i++)
                result += Math.Ceiling(R_i / hp_i[i].periode) * hp_i[i].WCET;

            return result;
        }

        public static double calcBlocking(List<task> tasks, int index)
        {
            task task_i = tasks[index];
            double result = 0, max = 0;

            int usage = Usage(tasks, index);
            
            if (usage == 0)
                return 0;           // The task can not be resource blocked

            // Calculate the maximum criticalBlockingTime for the semaphore
            for (int i = 0; i < tasks.Count; i++)
            {
                max = (tasks[i].blocking ? 1 : 0) * tasks[i].criticalBlockingTime;
                if (max > result)
                    result = max; 
            }

            return result;
        }

        public static int Usage(List<task> tasks, int index)
        {
            task task_i = tasks[index];
            bool higherOrEqual = false, lower = false;

            // Check if any higher or equal priority task shares the same samephore as the i'th task
            for (int i = 0; i <= index; i++)
            {
                if (tasks[i].blocking)
                {
                    higherOrEqual = true;
                    break;
                }
            }
            // Check if any lower priority task shares the same samephore as the i'th task
            for (int j = index +1; j < tasks.Count; j++)
            {
                if(tasks[j].blocking)
                {
                    lower = true;
                    break;
                }
            }
            if (higherOrEqual && lower) {
                tasks[index].usage = true;
                return 1;
            }
            else {
                return 0;
            }
        }

    }
}
