/* sample.cpp for TOPPERS/ATK(OSEK) */

// ECRobot++ API
#include "Nxt.h"
#include "Lcd.h"
#include "NxtColorSensor.h"
#include "Speaker.h"
#include "Clock.h"
using namespace ecrobot;

extern "C"
{
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"

    int readings = 1;

    char *map[8] = {
        "Black", "Blue", "Green", "Yellow", 
        "Orange", "Red", "White", "Unknown"
    };

    /* TOPPERS/ATK declarations */
    DeclareCounter(SysTimerCnt);
    DeclareAlarm(AlarmTask2);
    DeclareEvent(EventTask2);

    NxtColorSensor color(PORT_1); // default sensor mode: COLORSENSOR

	/* nxtOSEK hooks */
	void ecrobot_device_initialize()
	{
		ecrobot_init_bt_slave("LEJOS-OSEK");
		//ecrobot_init_nxtcolorsensor(NXT_PORT_S1, NXT_COLORSENSOR);
	}

	void ecrobot_device_terminate()
	{
		ecrobot_term_bt_connection();
	}

    // nxtOSEK hook to be invoked from an ISR in category 2
    void user_1ms_isr_type2(void)
    {
        (void)SignalCounter(SysTimerCnt); /* Increment OSEK Alarm Counter */
    }

    TASK(Task2)
    {
        Nxt nxt;
        Lcd lcd;
        Speaker speaker;
        Clock clock;
        S16 rgb[3];

        speaker.playTone(494, 200 , 10);
		color.processBackground();
        clock.wait(500);
        speaker.playTone(988, 200 , 10);

        while(1)
        {
            if(readings >= 20) {
                color.setSensorMode(NxtColorSensor::_DEACTIVATE); 
                readings = 0;
                nxt.restart();
            }

            WaitEvent(EventTask2);
            ClearEvent(EventTask2);
			

            color.getRawColor(rgb);
            
            // display different sensor data depending on the sensor mode
            lcd.clear();
            lcd.putf("s\n", "COLOR");
            lcd.putf("s", map[color.getColorNumber()]);
            lcd.putf("\nsd\nsd\nsd", "R:", rgb[0],0, "G:", rgb[1],0, "B:", rgb[2],0);
            lcd.cursor(0,6);
            lcd.putf("s\ns", "================", "ENTR:CHANGE MODE");
            lcd.putf("sd\n", "READINGS:", readings+1);
            lcd.putf("sd\n", "CLOCK:", clock.now());
            lcd.disp();
			ecrobot_bt_data_logger(1,color.getColorNumber());

            readings++;
        }
		speaker.playTone(50, 100, 10);
    }

    TASK(Task1)
    {
        SetRelAlarm(AlarmTask2, 1, 50); // set event for Task2 by Alarm
        while(1)
        {
            color.processBackground(); // communicates with NXT Color Sensor (this must be executed repeatedly in a background Task)
        }
    }
}
