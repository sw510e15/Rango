/* sample.cpp for TOPPERS/ATK(OSEK) */

// ECRobot++ API
#include "Nxt.h"
#include "Lcd.h"
#include "NxtColorSensor.h"
#include "Speaker.h"
#include "Clock.h"
#include "Motor.h"
using namespace ecrobot;

extern "C"
{
#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"

	S8 rotationSpeed = 100;
	U8 rounds = 10;

	Motor motorA(PORT_A);
    

    /* TOPPERS/ATK declarations */
    DeclareCounter(SysTimerCnt);
    DeclareAlarm(AlarmTask2);
    DeclareEvent(EventTask2);

    NxtColorSensor color(PORT_1); // default sensor mode: COLORSENSOR

	/* nxtOSEK hooks */
	void ecrobot_device_initialize()
	{
		ecrobot_init_bt_slave("LEJOS-OSEK");
		ecrobot_init_nxtcolorsensor(NXT_PORT_S1, NXT_COLORSENSOR);
	}

	void ecrobot_device_terminate()
	{
		ecrobot_term_bt_connection();
	}

    // nxtOSEK hook to be invoked from an ISR in category 2
    void user_1ms_isr_type2(void)
    {   //Increment OSEK Alarm Counter 
        (void)SignalCounter(SysTimerCnt); 
    }

    TASK(Task2)
    {
        Nxt nxt;
        Lcd lcd;
        Speaker speaker;
        Clock clock;


        speaker.playTone(494, 200 , 10);
		color.processBackground();
        clock.wait(500);
        speaker.playTone(988, 200 , 10);
		clock.wait(500);

        while(1)
        {
			if (motorA.getCount() >= 360 * rounds) {
				
				//To break the motor
				rotationSpeed = -10;
				rotationSpeed = 0;
				
				//Turn of sensor
				color.setSensorMode(NxtColorSensor::_DEACTIVATE);
				
				//Play end melody
				speaker.playTone(494, 200, 10);
				clock.wait(500);
				speaker.playTone(988, 200, 10);
				
				nxt.restart();
            }

            WaitEvent(EventTask2);
            ClearEvent(EventTask2);
			
			ecrobot_bt_data_logger(0,color.getColorNumber());

        }		
    }

    TASK(Task1)
    {    // set event for Task2 by Alarm
        SetRelAlarm(AlarmTask2, 1, 25);
        while(1)
        {
			motorA.setPWM(rotationSpeed);
			// communicates with NXT Color Sensor (this must be executed repeatedly in a background Task)
            color.processBackground(); 
        }
    }
}
