#pragma once
#ifndef BUCKET_ENUM    
#define BUCKET_ENUM    

enum Direction_t { clockwise = -1, counterclockwise = 1 };
enum Bucketstate_t { standby, settling, turning, tweaking };

#endif
