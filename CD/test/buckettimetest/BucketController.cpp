#include "BucketController.h"
#include "Bluetooth.h"
#include "Daq.h"

using namespace ecrobot;
Motor motor(PORT_A);
Clock clock;
Bluetooth bt;
Daq daq(bt);

// for datalogging
colour_t prevColour = UNKNOWN;
unsigned int startTime = 0;

Bucketstate_t state = standby;
colour_t currentColour = UNKNOWN;
Direction_t direction = counterclockwise;
int shortestDistance = 0;
int startAngle = 0;
unsigned int requiredSettleTime = 0;
bool motorStarted = false;
bool turnDone = false;
bool correctionTurnStarted = false;

BucketController::BucketController () {}
BucketController::~BucketController() {}

// ------
// Public
// ------

void BucketController::startTurn(colour_t c)
{
    // if the current colour is the same as the incomming colour
    // the bucket should not shift
    if (currentColour == c)
    {
        return;
    }
	
	// for datalogging
	prevColour = currentColour;
    startTime = clock.now();
	
	currentColour = c;
	resetTurnData();

    int targetAngle = getAngle(currentColour);
    startAngle = motor.getCount();
    shortestDistance = findShortestDistance(startAngle, targetAngle);
    direction = findDirection(startAngle, targetAngle, shortestDistance);

    motor.setPWM(MOTOR_PWM * direction);
    state = turning;
}
void BucketController::turnBucket()
{
    switch(state) 
    {
        case standby:
            return;
        case turning:
            hasTravelledDistance();
            break;
        case settling:
            isMotorSettled();
            break;
        // case tweaking:
            // if (!correctionTurnStarted)
            // {
                // startCorrectionTurn();
            // }

            // else if (correctionTurnDone()) 
            // {
                // motor.setPWM(0);
				
				// datalog();
				
                // resetTurnData();
                // return;
            // }
            // break;
        default:
            // oh no!
            break;
    }
}     


// -------
// Private
// -------

void BucketController::datalog()
{
	if (bt.isConnected())
	{
		S8 arr1[] = {0,prevColour}; // an owl :3
		U16 data1 = currentColour;
		S16 arr2[] = {0,0,0,0}; // that's a freaky owl!
		S32 arr3[] = {startTime,clock.now(),clock.now() - startTime,0};
		daq.send(arr1, data1, arr2, arr3);				
	}
}


void BucketController::hasTravelledDistance()
{      
    if (direction == counterclockwise) 
    {
        if (startAngle + shortestDistance <= motor.getCount() + BRAKELENGTH) 
        {
            motor.setPWM(0);
            requiredSettleTime = clock.now() + WAIT_TIME;
            state = settling;
        }
    } 
    else 
    {
        if (startAngle - shortestDistance >= motor.getCount() - BRAKELENGTH) 
        {
            motor.setPWM(0);
            requiredSettleTime = clock.now() + WAIT_TIME;
            state = settling;
        }
    }
}
int BucketController::findShortestDistance (int curr, int dest) 
{
    int rawDiff = curr > dest ? curr - dest : dest - curr;
    int modDiff = positive_modulo(rawDiff,360);
    return modDiff > 180 ? 360 - modDiff : modDiff;
}
int BucketController::getAngle(colour_t colour)
{
    switch (colour)
    {
        case RED:
            return RED_ANGLE;
        case GREEN:
            return GREEN_ANGLE;
        case BLUE:
            return BLUE_ANGLE;
        case UNKNOWN:
        default:
            return UNKNOWN_ANGLE;
    }
}
void BucketController::isMotorSettled()
{
    if (requiredSettleTime <= clock.now())  
    {
        state = tweaking;
		
		datalog();
    }
}
void BucketController::resetTurnData()
{
    direction = counterclockwise;
    correctionTurnStarted = false;
    state = standby;

    shortestDistance = 0;
    startAngle = 0;
    requiredSettleTime = 0;
}
void BucketController::startCorrectionTurn()
{
    correctionTurnStarted = true;

    int targetAngle = getAngle(currentColour);
    startAngle = motor.getCount();
    shortestDistance = findShortestDistance(startAngle, targetAngle);

    direction = findDirection(startAngle, targetAngle, shortestDistance);
    motor.setPWM(CORRECTION_PWM * direction);
}
bool BucketController::correctionTurnDone()
{
    if (direction == counterclockwise) 
    {
        if (startAngle + shortestDistance <= motor.getCount()) 
        {
            return true;
        }
    } 
    else 
    {
        if (startAngle - shortestDistance >= motor.getCount()) 
        {
            return true;
        }
    }

    return false;
}
Direction_t BucketController::findDirection (int startAngle, int destAngle, int shortestDistance) 
{
    if (positive_modulo(startAngle + shortestDistance, 360) == destAngle) 
        return counterclockwise;

    return clockwise;
}
inline int BucketController::positive_modulo(int i, int n) 
{
    return (i % n + n) % n;
}
//Only for debugging
const char* BucketController::colourToStr(colour_t c)
{
    switch (c)
    {
        case RED:
            return "Red";
        case GREEN:
            return "Green";
        case BLUE:
            return "Blue";
        case UNKNOWN:
        default:
            return "UNKNOWN";
    }
}
