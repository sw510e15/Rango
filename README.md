# Rango - LEGO Sorting Conveyor Belt  

**How to Flash and Compile from/on Windows**

Rango has already been compiled, and can be be flashed unto the NXT by running .rxeflash.sh file. 

To do this, please follow the guide on the website: http://lejos-osek.sourceforge.net/howtoupload.htm

All the files needed to compile nxtOSEK on windows are found in the nxtOSEK_setupfiles.zip or at http://puu.sh/lYZJv/8659c0fef1.zip

This includes nxtOSEK, NeXTTools, Cygwin x86(32bit), the libintl1 library, the libintl3 library, sg.exe, GNU-ARM 4.0.2

If error 127 occurs when compiling then add the lib files to your cygwin/bin folder

If you still can't transfer the files to the NXT then try and install the official LEGO software http://www.lego.com/en-US/mindstorms/downloads/nxt-software-download

**Directory Structure**

 - include: All project header files including third party files
 - spike: Quick ideas which are not part of final project
 - src: Source code of this application
 - test: All tests regarding hardware or software; Manual or automatic.

**Authors**

This project is made by a project group on Aalborg University studying software 
engineering on 5th semester. The group is named "sw510e15", and consists of the
following students:
 - Carsten Vestergaard Risager
 - Casper Møller Bartholomæussen
 - Kaj Printz Madsen
 - Martin Raunkjær Andersen
 - René Mejer Lauritsen
 - Rune Willum Larsen.
