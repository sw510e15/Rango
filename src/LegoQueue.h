#pragma once
#ifndef LEGOQUEUE_H
#define LEGOQUEUE_H
#define BRICKS_MEDIUM_CLOSE 290 // 325
#define BRICKS_SLOW_CLOSE 225 // 225
#include "Colour.h"
#include "LegoBrick.h"



namespace queue 
{    
    void enqueue(const LegoBrick brick);
    void enqueue(const colour_t colour, const int time);
    LegoBrick dequeue();
    bool isEmpty();
    int getLength();
    LegoBrick peek();
    LegoBrick peekNext();
	void closenessCheck();
	void bricksInQueueString(char* str);
	const char* colour_to_str(colour_t c);
}

#endif 
