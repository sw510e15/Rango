#include "ColourController.h"

using namespace ecrobot;

NxtColorSensor colourSensor(PORT_1, NxtColorSensor::_COLORSENSOR);

colour_t previousColour = UNKNOWN,
		 colourContinuesRead = UNKNOWN,
		 brickColour = UNKNOWN;
short colourCounter = 0;
int totalTaskTime = 0;
bool isSameBrick = false;

// ------
// Public
// ------

ColourController::ColourController() { }
ColourController::~ColourController() { }

colour_t ColourController::getObservedColour()
{
    return colourSensor.getColorNumber();
}


colour_t ColourController::getBrickColour()
{
    return brickColour;
}

bool ColourController::hasSeenNewBrick(colour_t obsColour, const int motorCount, int* brickFirstSeen)
{	
    if(!isAcceptedColour(obsColour)) 
    {
		*brickFirstSeen = 0;
        previousColour = UNKNOWN;
		isSameBrick = false;
        colourCounter = 0;
        return false;
    }

	//Because the sensor sees orange as red, yellow and white
	if(obsColour == ORANGE) 
    {
		// we would like to save the motorCount, in case this orange is the start of a new brick
		if (previousColour == UNKNOWN || previousColour == BLACK)
		{
			*brickFirstSeen = motorCount;
			previousColour = ORANGE;
		}
		// Orange never shows up in green or blue bricks, so prevcol is those, a new brick is started
		else if (previousColour == GREEN || previousColour == BLUE)
		{			
			if (isSameBrick)
			{
				*brickFirstSeen = motorCount;
			}
			isSameBrick = false;
			colourCounter = 0;
			previousColour = ORANGE;
		}
		else
		{
			obsColour = previousColour; 
		}
    }
	
	// We don't want orange bricks but orange are often the start of other bricks
	if (obsColour == ORANGE && colourCounter < 15) // if there are 15 consequtive orange readings, we give up and call it unknown
	{
		colourCounter++;		
		return false;
	}
    else if(obsColour == previousColour)
    {
		colourCounter++;		
        if(colourCounter >= COLOUR_COUNT_REDUNDANCY) // The right colour has been found
        {
			// see if it is the same brick as seen before
			if (isSameBrick) 
			{
				// check how long it has been on the belt
				int diff = motorCount - *brickFirstSeen;

				if (diff < BRICK_REV_LENGTH)
				{
					return false; // it is the same brick
				}
				else
				{
					 // new brick, seen now minus a constant, because the rev sum of a two brick sequence 
					 // is not the double of one bricks rev length for some reason.
					*brickFirstSeen = motorCount - 60;
				}
			}
			
			// else add the new brick
            brickColour = obsColour;
			isSameBrick = true;
            return true;
        }
        return false;
    }
    else // Different colour reading, then reset.
    {
		// the new brick is seen now
		if (isSameBrick || *brickFirstSeen == 0)
		{
			*brickFirstSeen = motorCount;
		}
		isSameBrick = false;
		
		// if prev is orange, we would like to keep the colourCount that has been saved
		if (previousColour != ORANGE)
		{
			colourCounter = 1;
		}
		else
		{
			colourCounter++;
		}
		
        previousColour = obsColour;
        
        return false;
    }
}

void ColourController::processBackround()
{
    colourSensor.processBackground();
}

// ------
// Private
// ------

bool ColourController::isAcceptedColour(colour_t colour)
{
    switch (colour)
    {
        case BLUE:
        case GREEN:
        case YELLOW:
        case RED:
        case WHITE:
        case ORANGE:
            return true;
        default:
            return false;
    }
}
