#include "BucketController.h"

using namespace ecrobot;

Motor motor(PORT_A);
Clock clock;

bucketstate_t state = STANDBY;
colour_t currentColour = UNKNOWN;
direction_t direction = COUNTERCLOCKWISE;
int shortestDistance = 0;
int startAngle = 0;
unsigned int requiredSettleTime = 0;
bool motorStarted = false;
bool turnDone = false;
bool correctionTurnStarted = false;

// ------
// Public
// ------

BucketController::BucketController() { }
BucketController::~BucketController() { }

void BucketController::startTurn(colour_t c)
{
    // if the current colour is the same as the incomming colour
    // the bucket should not shift
    if (currentColour == c)
    {
        return;
    }

    currentColour = c;
	resetTurnData();

    int targetAngle = getAngle(currentColour);
    startAngle = motor.getCount();
    shortestDistance = findShortestDistance(startAngle, targetAngle);
    direction = findDirection(startAngle, targetAngle, shortestDistance);

    motor.setPWM(MOTOR_PWM * direction);
    state = TURNING;
}

void BucketController::turnBucket()
{
    switch(state) 
    {
        case STANDBY:
            return;
        case TURNING:
            if(isFirstTurnDone()) {
                startSettling();
            }
            break;
        case SETTLING:
            if(isMotorSettled()) {
                startTweaking();
            }
            break;
        case TWEAKING:
            if (!correctionTurnStarted) {
                startCorrectionTurn();
            }

            else if (isCorrectionTurnDone()) {
                motor.setPWM(0);
                resetTurnData();
                return;
            }
            break;
        default:
            // oh no!
            break;
    }
}     


// -------
// Private
// -------

bool BucketController::isFirstTurnDone()
{      
    if (direction == COUNTERCLOCKWISE) 
    {
        return startAngle + shortestDistance <= motor.getCount() + BRAKELENGTH;
    } 
    else 
    {
        return startAngle - shortestDistance >= motor.getCount() - BRAKELENGTH;
    }
}

void BucketController::startSettling()
{
    motor.setPWM(0);
    requiredSettleTime = clock.now() + WAIT_TIME;
    state = SETTLING;
}


bool BucketController::isMotorSettled()
{
    return requiredSettleTime <= clock.now();
}

void BucketController::startTweaking()
{
    state = TWEAKING;
}

void BucketController::resetTurnData()
{
    direction = COUNTERCLOCKWISE;
    correctionTurnStarted = false;
    state = STANDBY;

    shortestDistance = 0;
    startAngle = 0;
    requiredSettleTime = 0;
}

void BucketController::startCorrectionTurn()
{
    correctionTurnStarted = true;

    int targetAngle = getAngle(currentColour);
    startAngle = motor.getCount();
    shortestDistance = findShortestDistance(startAngle, targetAngle);

    direction = findDirection(startAngle, targetAngle, shortestDistance);
    motor.setPWM(CORRECTION_PWM * direction);
}

bool BucketController::isCorrectionTurnDone()
{
    if (direction == COUNTERCLOCKWISE) 
    {
        if (startAngle + shortestDistance <= motor.getCount()) 
        {
            return true;
        }
    } 
    else 
    {
        if (startAngle - shortestDistance >= motor.getCount()) 
        {
            return true;
        }
    }

    return false;
}


int BucketController::findShortestDistance (int curr, int dest)
{
    int rawDiff = curr > dest ? curr - dest : dest - curr;
    int modDiff = positive_modulo(rawDiff,360);
    return modDiff > 180 ? 360 - modDiff : modDiff;
}


int BucketController::getAngle(colour_t colour)
{
    switch (colour)
    {
        case RED:
            return RED_ANGLE;
        case GREEN:
            return GREEN_ANGLE;
        case BLUE:
            return BLUE_ANGLE;
        case UNKNOWN:
        default:
            return UNKNOWN_ANGLE;
    }
}

direction_t BucketController::findDirection (int startAngle, int destAngle, int shortestDistance) 
{
    if (positive_modulo(startAngle + shortestDistance, 360) == destAngle) 
        return COUNTERCLOCKWISE;

    return CLOCKWISE;
}

inline int BucketController::positive_modulo(int i, int n) 
{
    return (i % n + n) % n;
}

//Only for debugging
const char* BucketController::colourToStr(colour_t c)
{
    switch (c)
    {
        case RED:
            return "Red";
        case GREEN:
            return "Green";
        case BLUE:
            return "Blue";
        case UNKNOWN:
        default:
            return "UNKNOWN";
    }
}
