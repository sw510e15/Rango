#include "BeltController.h"

using namespace ecrobot;

Motor primeMotor(PORT_B);
Motor secondMotor(PORT_C);

U8 motorSpeed = MOTOR_SPEED_FAST;
beltMode_t beltMode = FAST;
bool secondIsRunning = false;

// ------
// Public
// ------

BeltController::BeltController() { }
BeltController::~BeltController() { }

void BeltController::setSpeedFast()
{
	if (beltMode != FAST)
	{
		beltMode = FAST;
		motorSpeed = MOTOR_SPEED_FAST;
		updateMotors();
	}
}

void BeltController::setSpeedMedium()
{
	if (beltMode != MEDIUM)
	{
		beltMode = MEDIUM;
		motorSpeed = MOTOR_SPEED_MEDIUM;
		updateMotors();
	}
}

void BeltController::setSpeedSlow()
{
	if (beltMode != SLOW)
	{
		beltMode = SLOW;
		motorSpeed = MOTOR_SPEED_SLOW;
		updateMotors();
	}
}

void BeltController::startSecondMotor()
{
	secondIsRunning = true;
	secondMotor.setPWM(motorSpeed);
}

void BeltController::stopSecondMotor()
{
	secondMotor.setPWM(0);
	secondIsRunning = false;
}

void BeltController::startMotors(bool startSecond)
{
	startPrimeMotor();
	if(startSecond)
	{
		startSecondMotor();
	}
}

bool BeltController::isSecondMotorRunning()
{
	return secondIsRunning;
}

S32 BeltController::getMotorCount()
{
	return primeMotor.getCount();
}

// ------
// Private
// ------

void BeltController::updateMotors()
{
	primeMotor.setPWM(motorSpeed);
	if (secondIsRunning)
    {
        secondMotor.setPWM(motorSpeed);
    }
}

void BeltController::startPrimeMotor()
{
	primeMotor.setPWM(motorSpeed);
}
