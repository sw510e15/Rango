#pragma once
#ifndef BUCKET_CONTROLLER_H
#define BUCKET_CONTROLLER_H
#define UNKNOWN_ANGLE 0
#define RED_ANGLE 90
#define BLUE_ANGLE 180
#define GREEN_ANGLE 270
#define BRAKELENGTH 55
#define ERROR_MARGIN 3
#define MOTOR_PWM 100
#define CORRECTION_PWM 10 
#define WAIT_TIME 150

#include "Colour.h"
#include "Motor.h"
#include "Clock.h"

#include "BucketEnums.h"

class BucketController {
public:
	BucketController();
	~BucketController();
    
	void startTurn(colour_t c);
	void turnBucket();
    
private:
    bucketstate_t state;
	
    bool isFirstTurnDone();
    void startSettling();
    bool isMotorSettled();
    void startTweaking();
    void resetTurnData();
    void startCorrectionTurn();
	bool isCorrectionTurnDone();
	int findShortestDistance (int curr, int dest);
    int getAngle(colour_t colour);
	direction_t findDirection (int currentAngle, int destAngle, int shortestPath);
	inline int positive_modulo(int i, int n);
	const char* colourToStr(colour_t c);
};                                     

#endif // BUCKET_CONTROLLER
